﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using MongePio.Models;
using System.Web.Script.Serialization;

namespace MongePio.Controllers
{


    public class EmpeñoController : Controller
    {
        private MongePioEntities db = new MongePioEntities();

        // GET: Empeño
        public ActionResult Index()
        {
            var empeño = db.Empeño.Include(e => e.EstatusEmpeño).Include(e => e.Paquete).Include(e => e.Sucursal);
            return View(empeño.ToList());
        }

        // GET: Empeño/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empeño empeño = db.Empeño.Find(id);
            if (empeño == null)
            {
                return HttpNotFound();
            }
            return View(empeño);
        }

        // GET: Empeño/Create
        public ActionResult Create()
        {
            //NECESITAMOS QUITAR ESE SELCET LIST Y TOMAR LA SUCURSAL DEL USUARIO LOGEADO****
            Guid Sucursal = new Guid("5c747c58-a2d6-477a-be3f-92933a3cb83a");
            var SiguienteFolio = db.MongePio_GetSiguienteFolio(Sucursal).ToList().FirstOrDefault();
          //  var DuracionDisponible =db.MongePio_GetDuracionesPaqueteSucursal(Sucursal).ToList();
            var id = Guid.NewGuid();
            ViewBag.IdEstatus = new SelectList(db.EstatusEmpeño, "Id", "Estatus");
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre");
            ViewBag.IdDuracion = new SelectList(db.MongePio_GetDuracionesPaqueteSucursal(Sucursal).ToList(), "Id","Duracion");


            ViewBag.IdSucursal = new SelectList(db.Sucursal.Where(x=> x.IdSucursal==Sucursal), "IdSucursal", "Nombre");


            ViewBag.Folio = SiguienteFolio.FolioPrefijo;


            var empeño = new Empeño()
            {
                IdEmpeño = id 
                ,Folio= SiguienteFolio.FolioNumero
                ,Prueba=id
                ,Fecha= DateTime.Now
            };


            return View(empeño);
        }


        public JsonResult _GetClientes(string q)
        {
            //var res = db.UserTags.Where(x => x.Tag.Contains(q) && ((tipo.HasValue) ? x.TagType == tipo.Value : x.TagType > -1)).Take(10).Select(x => x.Tag).ToList();
            var Clientes = db.Clientes.Where(x => x.Nombre.Contains(q) || x.ApellidoPaterno.Contains(q) || x.ApellidoPaterno.Contains(q) || x.Direccion.Contains(q) || x.Telefono.Contains(q) || x.TelefonoCelular.Contains(q)).Take(8).OrderBy(x => x.Nombre).Select(x => new
            {
                value = x.Id,
                text = x.Nombre +" "+x.ApellidoPaterno+" "+x.ApellidoMaterno+ " - " + x.Direccion + " - " + x.Telefono + " - " + x.TelefonoCelular

            }).ToList();
            return Json(Clientes, JsonRequestBehavior.AllowGet);

        }

        public JsonResult _GetPreElecticos(string q , Guid z)
        {
            //var res = db.UserTags.Where(x => x.Tag.Contains(q) && ((tipo.HasValue) ? x.TagType == tipo.Value : x.TagType > -1)).Take(10).Select(x => x.Tag).ToList();
            var ayer = DateTime.Now.Date.AddDays(-1);
            var mañana = DateTime.Now.Date.AddDays(1);
            var Preelectricos = db.Prevaluo.Where(x => x.Nip.Contains(q) && x.FechaCaptura > ayer && x.FechaCaptura < mañana && x.IsDisponible==true && x.IdDuracion==z).Take(8).OrderBy(x => x.Nip).Select(x => new
            {
                value = x.Id,
                text = x.Nip + " " + x.CantidadArticulos + " Articulos Total:$" + x.Monto 

            }).ToList();

            return Json(Preelectricos, JsonRequestBehavior.AllowGet);

        }
        public JsonResult _GetInfoArticulos(Guid q)
        {
            //var res = db.UserTags.Where(x => x.Tag.Contains(q) && ((tipo.HasValue) ? x.TagType == tipo.Value : x.TagType > -1)).Take(10).Select(x => x.Tag).ToList();
            var InfoArticulos = db.MongePio_GetArticulosPrevaluo(q).ToList();

            return Json(InfoArticulos, JsonRequestBehavior.AllowGet);

        }


        public JsonResult _GetInfoClientes(Guid q)
        {
            //var res = db.UserTags.Where(x => x.Tag.Contains(q) && ((tipo.HasValue) ? x.TagType == tipo.Value : x.TagType > -1)).Take(10).Select(x => x.Tag).ToList();
            var InfoCliente = db.MongePio_GetInfoCliente(q).ToList();
                
            return Json(InfoCliente, JsonRequestBehavior.AllowGet);

        }




        // POST: Empeño/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Empeño empeño, List<DescripcionArticuloJoyeria> ArticulosJoyeria, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                GuardadoJoyeria joy = new GuardadoJoyeria();
                GuardadoElectricoPreEmpeño ele = new GuardadoElectricoPreEmpeño();

                if (ArticulosJoyeria !=null)
                {
                   joy= GuardarArticuloJoyeria(ArticulosJoyeria);
                }
                if (empeño.IdPrevaluo.HasValue)
                {
                      ele = AgregarArticuloElectrico(empeño.IdPrevaluo.Value, empeño.IdEmpeño);
               
                } 
                    empeño.Capital = ((joy !=null ) ? joy.MontoGarantia :0) + ((ele != null) ? ele.MontoGarantia : 0);
                


                db.Empeño.Add(empeño);
                db.SaveChanges();
                return RedirectToAction("Boleta", new { IdEmpeño = empeño.IdEmpeño });

            }

            ViewBag.IdEstatus = new SelectList(db.EstatusEmpeño, "Id", "Estatus", empeño.IdEstatus);
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre", empeño.IdPaquete);
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", empeño.IdSucursal);
            return View(empeño);
        }

        // GET: Empeño/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empeño empeño = db.Empeño.Find(id);
            if (empeño == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdEstatus = new SelectList(db.EstatusEmpeño, "Id", "Estatus", empeño.IdEstatus);
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre", empeño.IdPaquete);
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", empeño.IdSucursal);
            return View(empeño);
        }

        // POST: Empeño/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdEmpeño,IdSucursal,IdCliente,Folio,IsOro,Fecha,IdEstatus,IdPaquete,Capital,IdEmpeñoAnterior,IdClienteAnterior")] Empeño empeño)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empeño).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdEstatus = new SelectList(db.EstatusEmpeño, "Id", "Estatus", empeño.IdEstatus);
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre", empeño.IdPaquete);
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", empeño.IdSucursal);
            return View(empeño);
        }

        // GET: Empeño/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empeño empeño = db.Empeño.Find(id);
            if (empeño == null)
            {
                return HttpNotFound();
            }
            return View(empeño);
        }



        public PartialViewResult ArticuloJoyeria (Guid id, Guid IdDuracion)
        {
            var articuloJoyeria = new DescripcionArticuloJoyeria()
            {
                Id = Guid.NewGuid()
                ,IdEmpeño=id
            };
            var k =  db.Kilataje.Where(x => x.EnUso.Value).ToList();
            var precios = db.MongePio_GetKilatajes();
            var paquete = db.MongePio_GetInteres();

            var Cat = db.IdCategoriaJoyeria.Where(x=>x.EnUso.Value).ToList();
            var pqt = db.Paquete.Where(x => x.Activo.Value && x.IdDuracion == IdDuracion).ToList();
            var pidedra = db.Piedras.Where(x => x.IsActivo.Value).ToList();
            ViewBag.Kilatajes = new SelectList(k,"Id","Kilataje1"); 
            ViewBag.CategoriasJoyeria= new SelectList(Cat,"Id","Nombre");
            ViewBag.Paquetes = new SelectList(pqt,"IdPaquete","Nombre");
            ViewBag.Piedra = new SelectList(pidedra, "Id", "Nombre");
            ViewBag.precios = precios;
            ViewBag.paquete = paquete;



            return PartialView("ArticuloJoyeria",articuloJoyeria);
        }

        public PartialViewResult ArticuloElectrico(Guid id, Guid IdDuracion)
        {
            var articuloElectico = new DescripcionArticuloElectrico()
            {
                Id = Guid.NewGuid()
                ,
                IdEmpeño = id
            };
            //var k = db.Kilataje.Where(x => x.EnUso.Value).ToList();
            var Cat = db.IdCategoriaElectrico.Where(x => x.EnUso.Value).ToList();
            var pqt = db.Paquete.Where(x => x.Activo.Value && x.IsOro.Value == false).ToList();

            //ViewBag.Kilatajes = new SelectList(k, "Id", "Kilataje1");
            ViewBag.CategoriasElectrico = new SelectList(Cat, "Id", "Nombre");
            ViewBag.Paquetes = new SelectList(pqt, "IdPaquete", "Nombre");



            return PartialView("ArticuloElectrico", articuloElectico);
        }



        public GuardadoJoyeria GuardarArticuloJoyeria(List<DescripcionArticuloJoyeria> ArticulosJoyeria) {


            try
            {
                if (ArticulosJoyeria!=null && ArticulosJoyeria.Count > 0)
                {
                    double montoGarantia = 0;
                    foreach (var ArticuloJoyeria in ArticulosJoyeria)
                    {
                        var paquete = db.Paquete.Where(x => x.IdPaquete == ArticuloJoyeria.IdPaquete).FirstOrDefault();


                        ArticuloJoyeria.FechaVencimiento = (ArticuloJoyeria.FechaInicio.Value.AddDays(paquete.Duracion.Value));
                        ArticuloJoyeria.FechaMoratorio = (ArticuloJoyeria.FechaInicio.Value.AddDays(paquete.DuracionMoratorio.Value));
                        ArticuloJoyeria.FechaRemate = (ArticuloJoyeria.FechaMoratorio.Value.AddDays(paquete.DuracionRemate.Value));
                        ArticuloJoyeria.MontoVenta = (ArticuloJoyeria.MontoPrestamo); //aqui agregar si se va a poner un porcentaje por venta

                        montoGarantia = montoGarantia + ArticuloJoyeria.MontoPrestamo.Value;

                        db.DescripcionArticuloJoyeria.Add(ArticuloJoyeria);
                    }

                      db.SaveChanges();
                    return (new GuardadoJoyeria {Estatus= true,MontoGarantia=montoGarantia });
                   

                }
                else {
                    return (new GuardadoJoyeria { Estatus = false, MontoGarantia = 0 });

                }


            }
            catch (Exception e)
            {
                var x = e.Message;
                return (new GuardadoJoyeria { Estatus = false, MontoGarantia = 0 });
                throw;
            }


            return (new GuardadoJoyeria { Estatus = false, MontoGarantia = 0 });
        }


        public GuardadoElectricoPreEmpeño AgregarArticuloElectrico(Guid Prevaluo,Guid IdEmpeño)
        {


            try
            {
                List<DescripcionArticuloElectrico> ArticulosElectrico = db.DescripcionArticuloElectrico.Where(x => x.IdPreEmpeño == Prevaluo).ToList();


                if (ArticulosElectrico.Count>0)
                {
                    double montoGarantia = 0;


                    foreach (var ArticuloElectrico in ArticulosElectrico)
                    {
                        var paquete = db.Paquete.Where(x => x.IdPaquete == ArticuloElectrico.IdPaquete).FirstOrDefault();

                        ArticuloElectrico.IdEmpeño = IdEmpeño;
                        ArticuloElectrico.FechaInicio = DateTime.Now.Date;
                        ArticuloElectrico.FechaVencimiento = (ArticuloElectrico.FechaInicio.Value.Date.AddDays(paquete.Duracion.Value));
                        ArticuloElectrico.FechaMoratorio = (ArticuloElectrico.FechaInicio.Value.Date.AddDays(paquete.DuracionMoratorio.Value));
                        ArticuloElectrico.FechaRemate = (ArticuloElectrico.FechaInicio.Value.Date.AddDays(paquete.DuracionRemate.Value));
                        ArticuloElectrico.MontoVenta = (ArticuloElectrico.MontoPrestamo); //aqui agregar si se va a poner un porcentaje por venta

                        montoGarantia = montoGarantia + ArticuloElectrico.MontoPrestamo.Value;

                      
                    }
                    var preva = db.Prevaluo.Where(x=>x.Id== Prevaluo).FirstOrDefault();
                    if (preva != null)
                    {
                        preva.IsDisponible = false;
                    }

                    db.SaveChanges();
                    return (new GuardadoElectricoPreEmpeño{ Estatus = true, MontoGarantia = montoGarantia });


                }
                else
                {
                    return (new GuardadoElectricoPreEmpeño { Estatus = true, MontoGarantia = 0 });

                }


            }
            catch (Exception e)
            {
                var x = e.Message;
                return (new GuardadoElectricoPreEmpeño { Estatus = true, MontoGarantia = 0 });
                throw;
            }


            return (new GuardadoElectricoPreEmpeño { Estatus = true, MontoGarantia = 0 });
        }

        // POST: Empeño/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Empeño empeño = db.Empeño.Find(id);
            db.Empeño.Remove(empeño);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult Boleta(Guid IdEmpeño)
        {

           
            Boleta Boleta = new Boleta();
            Boleta.Encabezado = db.MongePio_Boleta_GetDatosGenerales(IdEmpeño).FirstOrDefault();
            Boleta.ArticulosJoyeria = db.MongePio_Boleta_GetArticulosJoyeria(IdEmpeño).ToList();
            Boleta.ArticulosElectricos = db.MongePio_Boleta_GetArticulosElectico(IdEmpeño).ToList();


            return View(Boleta);

        }



    }

        


}
