﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class EmpleadoController : Controller
    {
        private MongePioEntities db = new MongePioEntities();

        // GET: Empleado
        public ActionResult Index()
        {
            var empleado = db.Empleado.Include(e => e.Users).Include(e => e.Puesto).Include(e => e.Sucursal);
            return View(empleado.ToList());
        }

        // GET: Empleado/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = db.Empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // GET: Empleado/Create
        public ActionResult Create()
        {
            ViewBag.IdUsuario = new SelectList(db.Users, "IdUser", "Username");
            ViewBag.IdPuesto = new SelectList(db.Puesto, "Id", "Puesto1");
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre");
            return View();
        }

        // POST: Empleado/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdUsuario,IdPuesto,IdSucursal,IsGlobal")] Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                empleado.Id = Guid.NewGuid();
                db.Empleado.Add(empleado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdUsuario = new SelectList(db.Users, "IdUser", "Username", empleado.IdUsuario);
            ViewBag.IdPuesto = new SelectList(db.Puesto, "Id", "Puesto1", empleado.IdPuesto);
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", empleado.IdSucursal);
            return View(empleado);
        }

        // GET: Empleado/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = db.Empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdUsuario = new SelectList(db.Users, "IdUser", "Username", empleado.IdUsuario);
            ViewBag.IdPuesto = new SelectList(db.Puesto, "Id", "Puesto1", empleado.IdPuesto);
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", empleado.IdSucursal);
            return View(empleado);
        }

        // POST: Empleado/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdUsuario,IdPuesto,IdSucursal,IsGlobal")] Empleado empleado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empleado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdUsuario = new SelectList(db.Users, "IdUser", "Username", empleado.IdUsuario);
            ViewBag.IdPuesto = new SelectList(db.Puesto, "Id", "Puesto1", empleado.IdPuesto);
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", empleado.IdSucursal);
            return View(empleado);
        }

        // GET: Empleado/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empleado empleado = db.Empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // POST: Empleado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Empleado empleado = db.Empleado.Find(id);
            db.Empleado.Remove(empleado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
