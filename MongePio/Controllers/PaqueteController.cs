﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class PaqueteController : Controller
    {
        private MongePioEntities db = new MongePioEntities();

        // GET: Paquete
        public ActionResult Index()
        {
            var paquete = db.Paquete.Include(p => p.Sucursal);
            return View(paquete.ToList());
        }

        // GET: Paquete/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paquete paquete = db.Paquete.Find(id);
            if (paquete == null)
            {
                return HttpNotFound();
            }
            return View(paquete);
        }

        // GET: Paquete/Create
        public ActionResult Create()
        {
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre");
            return View();
        }

        // POST: Paquete/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPaquete,Nombre,IdSucursal,IsGlobal,Interes,InteresMoratorio,InteresRemate,Duracion,DuracionMoratorio,DuracionRemate,Activo,FechaAlta,FechaBaja")] Paquete paquete)
        {
            if (ModelState.IsValid)
            {
                paquete.IdPaquete = Guid.NewGuid();
                db.Paquete.Add(paquete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", paquete.IdSucursal);
            return View(paquete);
        }

        // GET: Paquete/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paquete paquete = db.Paquete.Find(id);
            if (paquete == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", paquete.IdSucursal);
            return View(paquete);
        }

        // POST: Paquete/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPaquete,Nombre,IdSucursal,IsGlobal,Interes,InteresMoratorio,InteresRemate,Duracion,DuracionMoratorio,DuracionRemate,Activo,FechaAlta,FechaBaja")] Paquete paquete)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paquete).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", paquete.IdSucursal);
            return View(paquete);
        }

        // GET: Paquete/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paquete paquete = db.Paquete.Find(id);
            if (paquete == null)
            {
                return HttpNotFound();
            }
            return View(paquete);
        }

        // POST: Paquete/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Paquete paquete = db.Paquete.Find(id);
            db.Paquete.Remove(paquete);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
