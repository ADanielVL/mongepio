﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class ColoniasController : Controller
    {
        private MongePioEntities  db = new MongePioEntities();

        // GET: Colonias
        public ActionResult Index()
        {
            return View(db.Colonia.ToList());
        }

        // GET: Colonias/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Colonia colonia = db.Colonia.Find(id);
            if (colonia == null)
            {
                return HttpNotFound();
            }
            return View(colonia);
        }

        // GET: Colonias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Colonias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdPais,IdEstado,IdCiudad,Nombre,CodigoPostal")] Colonia colonia)
        {
            if (ModelState.IsValid)
            {
                colonia.Id = Guid.NewGuid();
                db.Colonia.Add(colonia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(colonia);
        }

        // GET: Colonias/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Colonia colonia = db.Colonia.Find(id);
            if (colonia == null)
            {
                return HttpNotFound();
            }
            return View(colonia);
        }

        // POST: Colonias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdPais,IdEstado,IdCiudad,Nombre,CodigoPostal")] Colonia colonia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(colonia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(colonia);
        }

        // GET: Colonias/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Colonia colonia = db.Colonia.Find(id);
            if (colonia == null)
            {
                return HttpNotFound();
            }
            return View(colonia);
        }

        // POST: Colonias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Colonia colonia = db.Colonia.Find(id);
            db.Colonia.Remove(colonia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
