﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class ClientesController : Controller
    {
        private MongePioEntities db = new MongePioEntities();

        // GET: Clientes
        public ActionResult Index()
        {
            var clientes = db.Clientes.Include(c => c.Estado).Include(c => c.Pais).Include(c => c.Sucursal);
            return View(clientes.ToList());
        }

        // GET: Clientes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clientes clientes = db.Clientes.Find(id);
            if (clientes == null)
            {
                return HttpNotFound();
            }
            return View(clientes);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            ViewBag.IdEstado = new SelectList(db.Estado, "ID", "Nombre");
            ViewBag.IdPais = new SelectList(db.Pais, "Id", "Nombre");
            ViewBag.IdSucursalAnterior = new SelectList(db.Sucursal, "IdSucursal", "Nombre");
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,ApellidoPaterno,ApellidoMaterno,IdPais,IdEstado,IdCiudad,IdColonia,Direccion,Telefono,TelefonoCelular,Correo,EstadoCivil,IdSucursalAnterior,IdClienteAnterior,IdColoniaAnterior")] Clientes clientes)
        {
            if (ModelState.IsValid)
            {
                clientes.Id = Guid.NewGuid();
                db.Clientes.Add(clientes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdEstado = new SelectList(db.Estado, "ID", "Nombre", clientes.IdEstado);
            ViewBag.IdPais = new SelectList(db.Pais, "Id", "Nombre", clientes.IdPais);
            ViewBag.IdSucursalAnterior = new SelectList(db.Sucursal, "IdSucursal", "Nombre", clientes.IdSucursalAnterior);
            return View(clientes);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clientes clientes = db.Clientes.Find(id);
            if (clientes == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdEstado = new SelectList(db.Estado, "ID", "Nombre", clientes.IdEstado);
            ViewBag.IdPais = new SelectList(db.Pais, "Id", "Nombre", clientes.IdPais);
            ViewBag.IdSucursalAnterior = new SelectList(db.Sucursal, "IdSucursal", "Nombre", clientes.IdSucursalAnterior);
            return View(clientes);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,ApellidoPaterno,ApellidoMaterno,IdPais,IdEstado,IdCiudad,IdColonia,Direccion,Telefono,TelefonoCelular,Correo,EstadoCivil,IdSucursalAnterior,IdClienteAnterior,IdColoniaAnterior")] Clientes clientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdEstado = new SelectList(db.Estado, "ID", "Nombre", clientes.IdEstado);
            ViewBag.IdPais = new SelectList(db.Pais, "Id", "Nombre", clientes.IdPais);
            ViewBag.IdSucursalAnterior = new SelectList(db.Sucursal, "IdSucursal", "Nombre", clientes.IdSucursalAnterior);
            return View(clientes);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clientes clientes = db.Clientes.Find(id);
            if (clientes == null)
            {
                return HttpNotFound();
            }
            return View(clientes);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Clientes clientes = db.Clientes.Find(id);
            db.Clientes.Remove(clientes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
