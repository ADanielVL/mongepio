﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class PrevaluosController : Controller
    {
        private MongePioEntities db = new MongePioEntities();

        // GET: Prevaluos
        public ActionResult Index()
        {
            var prevaluo = db.Prevaluo.Include(p => p.Sucursal);
            return View(prevaluo.ToList());
        }

        // GET: Prevaluos/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prevaluo prevaluo = db.Prevaluo.Find(id);
            if (prevaluo == null)
            {
                return HttpNotFound();
            }
            return View(prevaluo);
        }

        // GET: Prevaluos/Create
        public ActionResult Create()
        {
            //NECESITAMOS QUITAR ESE SELCET LIST Y TOMAR LA SUCURSAL DEL USUARIO LOGEADO****
            Guid Sucursal = new Guid("5c747c58-a2d6-477a-be3f-92933a3cb83a");
            var id = Guid.NewGuid();
            ViewBag.IdSucursal = new SelectList(db.Sucursal.Where(x => x.IdSucursal == Sucursal), "IdSucursal", "Nombre");
            ViewBag.IdDuracion = new SelectList(db.MongePio_GetDuracionesPaqueteSucursal(Sucursal).ToList(), "Id", "Duracion");

            var prevaluo = new Prevaluo() { 
            Id= id
            ,IdSucursal=Sucursal
            ,IdUsuaroCaptura = new Guid("D0A72435-58C2-4C75-A723-CF30B24FF8CF") //aqui tenemos que poner el idUsuarioLogeado
            };

            return View(prevaluo);
        }
        public PartialViewResult ArticuloElectrico(Guid id, Guid IdDuracion)
        {
              
            var Cat = db.IdCategoriaElectrico.Where(x => x.EnUso.Value).ToList();
            var pqt = db.Paquete.Where(x => x.Activo.Value && x.IsOro.Value == false && x.IdDuracion==IdDuracion).ToList();
            ViewBag.CategoriasElectrico = new SelectList(Cat, "Id", "Nombre");
            ViewBag.Paquetes = new SelectList(pqt, "IdPaquete", "Nombre");
            var articuloElectico = new DescripcionArticuloElectrico()
            {
                Id = Guid.NewGuid()
                ,
                IdPreEmpeño = id
            };

            return PartialView("ArticuloElectrico", articuloElectico);
        }

        public GuardadoElectricoPreEmpeño GuardarArticuloElectrico(List<DescripcionArticuloElectrico> ArticuloElectricos) {
            try
            {
                if (ArticuloElectricos.Count > 0)
                {
                    double montoGarantia = 0;
                    int CantArticulos = 0;
                    foreach (var ArticuloElectrico in ArticuloElectricos)
                    {

                        ArticuloElectrico.Id = Guid.NewGuid();
                        ArticuloElectrico.IdEstatusVenta = new Guid("C9CF8DEE-F29A-473F-B83A-DDA87113C5D6");
                        ArticuloElectrico.IdSucursal = new Guid("5c747c58-a2d6-477a-be3f-92933a3cb83a");


                        montoGarantia = montoGarantia + ArticuloElectrico.MontoPrestamo.Value;
                        CantArticulos = CantArticulos +1;
                        db.DescripcionArticuloElectrico.Add(ArticuloElectrico);
                    }

                    db.SaveChanges();
                    return (new GuardadoElectricoPreEmpeño { Estatus = true, MontoGarantia = montoGarantia, Atriculos = CantArticulos });


                }
                else
                {
                    return (new GuardadoElectricoPreEmpeño { Estatus = false, MontoGarantia = 0, Atriculos = 0 });

                }


            }
            catch (Exception e)
            {
                var x = e.Message;
                return (new GuardadoElectricoPreEmpeño { Estatus = false, MontoGarantia = 0, Atriculos = 0 });
                throw;
            }


            return (new GuardadoElectricoPreEmpeño { Estatus = false, MontoGarantia = 0,Atriculos=0 });
        }



        // POST: Prevaluos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create( Prevaluo prevaluo, List<DescripcionArticuloElectrico> ArticuloElectrico, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var Articulos = GuardarArticuloElectrico(ArticuloElectrico);
                #region CrearNip
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var random = new Random();

                bool nipvalido = false;
                String nip = "G";
                var length = 5;
                var loop = 100000;
                var cont = 0;
                var result = new string(
                    Enumerable.Repeat(chars, length)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());
                while (!nipvalido)
                {
                    cont++;
                    if (cont > loop)
                    {
                        result = new string(
                        Enumerable.Repeat(chars, length + (cont / loop))
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());
                    }
                    nip = "G" + result;
                    if (db.Prevaluo.Count(x => x.Nip == nip) == 0)
                        nipvalido = true;
                }
                prevaluo.Nip = result;
                #endregion
                prevaluo.FechaCaptura = DateTime.Now.Date;
                prevaluo.IsDisponible = true;
                prevaluo.CantidadArticulos = Articulos.Atriculos;
                prevaluo.Monto = Articulos.MontoGarantia;
                
                db.Prevaluo.Add(prevaluo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", prevaluo.IdSucursal);
            return View(prevaluo);
        }

        // GET: Prevaluos/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prevaluo prevaluo = db.Prevaluo.Find(id);
            if (prevaluo == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", prevaluo.IdSucursal);
            return View(prevaluo);
        }

        // POST: Prevaluos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdUsuaroCaptura,IdSucursal,Nip,FechaCaptura,IsDisponible")] Prevaluo prevaluo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prevaluo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", prevaluo.IdSucursal);
            return View(prevaluo);
        }

        // GET: Prevaluos/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prevaluo prevaluo = db.Prevaluo.Find(id);
            if (prevaluo == null)
            {
                return HttpNotFound();
            }
            return View(prevaluo);
        }

        // POST: Prevaluos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Prevaluo prevaluo = db.Prevaluo.Find(id);
            db.Prevaluo.Remove(prevaluo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
