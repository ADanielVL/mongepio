﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class DescripcionArticuloJoyeriasController : Controller
    {
        private MongePioEntities db = new MongePioEntities();

        // GET: DescripcionArticuloJoyerias
        public ActionResult Index()
        {
            var descripcionArticuloJoyeria = db.DescripcionArticuloJoyeria.Include(d => d.IdCategoriaJoyeria1).Include(d => d.Paquete);
            return View(descripcionArticuloJoyeria.ToList());
        }

        // GET: DescripcionArticuloJoyerias/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DescripcionArticuloJoyeria descripcionArticuloJoyeria = db.DescripcionArticuloJoyeria.Find(id);
            if (descripcionArticuloJoyeria == null)
            {
                return HttpNotFound();
            }
            return View(descripcionArticuloJoyeria);
        }

        // GET: DescripcionArticuloJoyerias/Create
        public ActionResult Create()
        {
            ViewBag.IdCategoriaJoyeria = new SelectList(db.IdCategoriaJoyeria, "Id", "Nombre");
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre");
            return View();
        }

        // POST: DescripcionArticuloJoyerias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdKilataje,Descripcion,Peso,IdCategoriaJoyeria,IdEstatusVenta,IsVendido,IdSucursal,MontoVenta,IdDescripcionVieja,IdPaquete")] DescripcionArticuloJoyeria descripcionArticuloJoyeria)
        {
            if (ModelState.IsValid)
            {
                descripcionArticuloJoyeria.Id = Guid.NewGuid();
                db.DescripcionArticuloJoyeria.Add(descripcionArticuloJoyeria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCategoriaJoyeria = new SelectList(db.IdCategoriaJoyeria, "Id", "Nombre", descripcionArticuloJoyeria.IdCategoriaJoyeria);
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre", descripcionArticuloJoyeria.IdPaquete);
            return View(descripcionArticuloJoyeria);
        }

        // GET: DescripcionArticuloJoyerias/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DescripcionArticuloJoyeria descripcionArticuloJoyeria = db.DescripcionArticuloJoyeria.Find(id);
            if (descripcionArticuloJoyeria == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCategoriaJoyeria = new SelectList(db.IdCategoriaJoyeria, "Id", "Nombre", descripcionArticuloJoyeria.IdCategoriaJoyeria);
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre", descripcionArticuloJoyeria.IdPaquete);
            return View(descripcionArticuloJoyeria);
        }

        // POST: DescripcionArticuloJoyerias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdKilataje,Descripcion,Peso,IdCategoriaJoyeria,IdEstatusVenta,IsVendido,IdSucursal,MontoVenta,IdDescripcionVieja,IdPaquete")] DescripcionArticuloJoyeria descripcionArticuloJoyeria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(descripcionArticuloJoyeria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCategoriaJoyeria = new SelectList(db.IdCategoriaJoyeria, "Id", "Nombre", descripcionArticuloJoyeria.IdCategoriaJoyeria);
            ViewBag.IdPaquete = new SelectList(db.Paquete, "IdPaquete", "Nombre", descripcionArticuloJoyeria.IdPaquete);
            return View(descripcionArticuloJoyeria);
        }

        // GET: DescripcionArticuloJoyerias/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DescripcionArticuloJoyeria descripcionArticuloJoyeria = db.DescripcionArticuloJoyeria.Find(id);
            if (descripcionArticuloJoyeria == null)
            {
                return HttpNotFound();
            }
            return View(descripcionArticuloJoyeria);
        }

        // POST: DescripcionArticuloJoyerias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            DescripcionArticuloJoyeria descripcionArticuloJoyeria = db.DescripcionArticuloJoyeria.Find(id);
            db.DescripcionArticuloJoyeria.Remove(descripcionArticuloJoyeria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
