﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class PuestoesController : Controller
    {
        private MongePioEntities db = new MongePioEntities();

        // GET: Puestoes
        public ActionResult Index()
        {
            var puesto = db.Puesto.Include(p => p.Sucursal);
            return View(puesto.ToList());
        }

        // GET: Puestoes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Puesto puesto = db.Puesto.Find(id);
            if (puesto == null)
            {
                return HttpNotFound();
            }
            return View(puesto);
        }

        // GET: Puestoes/Create
        public ActionResult Create()
        {
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre");
            return View();
        }

        // POST: Puestoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdSucursal,Puesto1,IsGlobal")] Puesto puesto)
        {
            if (ModelState.IsValid)
            {
                puesto.Id = Guid.NewGuid();
                db.Puesto.Add(puesto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", puesto.IdSucursal);
            return View(puesto);
        }

        // GET: Puestoes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Puesto puesto = db.Puesto.Find(id);
            if (puesto == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", puesto.IdSucursal);
            return View(puesto);
        }

        // POST: Puestoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdSucursal,Puesto1,IsGlobal")] Puesto puesto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(puesto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdSucursal = new SelectList(db.Sucursal, "IdSucursal", "Nombre", puesto.IdSucursal);
            return View(puesto);
        }

        // GET: Puestoes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Puesto puesto = db.Puesto.Find(id);
            if (puesto == null)
            {
                return HttpNotFound();
            }
            return View(puesto);
        }

        // POST: Puestoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Puesto puesto = db.Puesto.Find(id);
            db.Puesto.Remove(puesto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
