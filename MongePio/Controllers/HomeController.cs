﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongePio.Models;

namespace MongePio.Controllers
{
    public class HomeController : Controller
    {
        MongePioEntities db = new MongePioEntities();

        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                Guid userid = new Guid(Session["UserID"].ToString());
                Session.Timeout = 1440;
                Session["ListSucursal"] = db.Sucursal.ToList();
                ViewBag.Rol = db.UserRoleMapping.Where(x => x.UserId == userid).Select(y => y.Roles.RoleName).FirstOrDefault();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult CambioSucursal()
        {
            ViewBag.ListSucursal = db.Sucursal.ToList();
            return Content("ok");
        }

        [HttpPost]
        public ActionResult CambioSucursal(Guid IdSucursal)
        {
            Session["IdSucursal"] = IdSucursal;
            return Content("ok");
        }
    }
}