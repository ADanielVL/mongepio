﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MongePio.Startup))]
namespace MongePio
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
