//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MongePio.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Paquete
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Paquete()
        {
            this.DescripcionArticuloJoyeria = new HashSet<DescripcionArticuloJoyeria>();
            this.DescripcionArticuloElectrico = new HashSet<DescripcionArticuloElectrico>();
        }
    
        public System.Guid IdPaquete { get; set; }
        public string Nombre { get; set; }
        public Nullable<System.Guid> IdSucursal { get; set; }
        public Nullable<bool> IsGlobal { get; set; }
        public Nullable<bool> IsOro { get; set; }
        public Nullable<double> Interes { get; set; }
        public Nullable<double> InteresMoratorio { get; set; }
        public Nullable<double> InteresRemate { get; set; }
        public Nullable<int> Duracion { get; set; }
        public Nullable<int> DuracionMoratorio { get; set; }
        public Nullable<int> DuracionRemate { get; set; }
        public Nullable<bool> Activo { get; set; }
        public Nullable<System.DateTime> FechaAlta { get; set; }
        public Nullable<System.DateTime> FechaBaja { get; set; }
        public Nullable<int> IdPaqueteViejo { get; set; }
        public Nullable<System.Guid> IdDuracion { get; set; }
    
        public virtual DuracionPaquetes DuracionPaquetes { get; set; }
        public virtual Sucursal Sucursal { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DescripcionArticuloJoyeria> DescripcionArticuloJoyeria { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Empeño> Empeño { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DescripcionArticuloElectrico> DescripcionArticuloElectrico { get; set; }
    }
}
