//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MongePio.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Clientes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Clientes()
        {
            this.Apartado = new HashSet<Apartado>();
            this.Colaborador = new HashSet<Colaborador>();
            this.Colaborador1 = new HashSet<Colaborador>();
        }
    
        public System.Guid Id { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public Nullable<System.Guid> IdPais { get; set; }
        public Nullable<System.Guid> IdEstado { get; set; }
        public Nullable<System.Guid> IdCiudad { get; set; }
        public Nullable<System.Guid> IdColonia { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string TelefonoCelular { get; set; }
        public string Correo { get; set; }
        public string EstadoCivil { get; set; }
        public Nullable<System.Guid> IdSucursalAnterior { get; set; }
        public Nullable<int> IdClienteAnterior { get; set; }
        public Nullable<int> IdColoniaAnterior { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Apartado> Apartado { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual Sucursal Sucursal { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Colaborador> Colaborador { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Colaborador> Colaborador1 { get; set; }
    }
}
