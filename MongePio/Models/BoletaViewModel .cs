﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MongePio.Models
{
    public class Boleta
    {
        
        public MongePio_Boleta_GetDatosGenerales_Result Encabezado { get; set; }
        public List<MongePio.Models.MongePio_Boleta_GetArticulosJoyeria_Result> ArticulosJoyeria { get; set; }

        public  List<MongePio.Models.MongePio_Boleta_GetArticulosElectico_Result> ArticulosElectricos { get; set; }


    }


}
