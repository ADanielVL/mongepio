//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MongePio.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Colonia
    {
        public System.Guid Id { get; set; }
        public Nullable<System.Guid> IdPais { get; set; }
        public Nullable<System.Guid> IdEstado { get; set; }
        public Nullable<System.Guid> IdCiudad { get; set; }
        public string Nombre { get; set; }
        public string CodigoPostal { get; set; }
    
        public virtual Ciudad Ciudad { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual Pais Pais { get; set; }
    }
}
