﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MongePio.Models
{
    public class GuardadoJoyeria
    {
        public bool Estatus { get; set; }
        public double MontoGarantia { get; set; }

    }
    public class GuardadoElectricoPreEmpeño
    {
        public bool Estatus { get; set; }
        public double MontoGarantia { get; set; }
        public int Atriculos { get; set; }

    }

}
